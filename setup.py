#!/usr/bin/env python

import os, sys
import yaml, simplejson as json

if __name__=='__main__':
    ENV_FILE  = open('%(UNIT_PATH)s/env.sh'   % os.environ, 'w+')
    RUN_FILE  = open('%(UNIT_PATH)s/run.sh'   % os.environ, 'w+')
    PROC_FILE = open('%(UNIT_PATH)s/Procfile' % os.environ, 'w+')

    ############################################################################

    for key in ['ACTIONS',None]+[
        'UNIT_' + x
        for x in ('NAME', 'PATH', 'SPECS', 'CONFIG', 'DEPLOY')
    ]+[None]:
        if key is None:
            ENV_FILE.write('\n')
            RUN_FILE.write('\n')
        elif key in os.environ:
            ENV_FILE.write('export %s=%s\n' % (key, os.environ[key]))
            RUN_FILE.write('export %s=%s\n' % (key, os.environ[key]))

    RUN_FILE.write('''#!/bin/sh

''' % os.environ)

    ############################################################################

    CONFIG = dict([
        (key, yaml.load(open('%s/%s.yml' % (os.environ['UNIT_CONFIG'], key)).read()) or {})
        for key in ('neurochip', 'deploy', 'workers')
    ])

    ############################################################################

    for key,value in CONFIG['deploy']['env'].iteritems():
        ENV_FILE.write('%s=%s\n' % (key, value))

    ############################################################################

    for alias,worker in CONFIG['workers'].iteritems():
        if 'process' in worker:
            for key in worker['process']:
                PROC_FILE.write('%s_%s: %s\n' % (alias, key, worker['process'][key]['command']))
        elif 'command' in worker:
            PROC_FILE.write('%s: %s\n' % (alias, worker['command']))

    ############################################################################

    for cfg in CONFIG['neurochip'].get('ghost', []):
        PROC_FILE.write('ghost-%(alias)s: nodejs main.js ghost://self/%(module)s\n' % cfg)

    for cfg in CONFIG['neurochip'].get('brain', []):
        PROC_FILE.write('ghost-%(narrow)s: nodejs main.js brain://self/%(adapter)s?ann=%(topology)s\n' % cfg)

    for cfg in CONFIG['neurochip'].get('organs', []):
        PROC_FILE.write('organ-%(module)s-%(uuid)s: nodejs main.js organ://self/%(module)s?uuid=%(uuid)s\n' % cfg)

    ############################################################################

    RUN_FILE.write('''
cd %(UNIT_PATH)s

npm install

foreman start

read
''' % os.environ)

    ############################################################################

    for f in (ENV_FILE, RUN_FILE, PROC_FILE):
        f.close()
