var URL = require('url');
    Util = require('util');

exports.specs = URL.parse(process.argv[2], true);

//console.log(Util.inspect(exports.specs));

exports.tenancy = {
  identity: exports.specs.auth,
  domain:   exports.specs.host,
  protocol: exports.specs.protocol.substr(0, exports.specs.protocol.length-1),
  module:   exports.specs.pathname.substr(1),
  params:   exports.specs.query,
};

if (exports.tenancy.domain=='self') {
  exports.tenancy.identity = process.env.UNIT_NAME || 'shivha';
  exports.tenancy.domain = process.env.UNIT_FQDN || 'uchikoma.zone';
}

if (exports.tenancy.identity) {
  exports.tenancy.identity = process.env.UNIT_NAME;
}

if (exports.tenancy.domain) {
  exports.tenancy.domain = process.env.UNIT_FQDN || 'uchikoma.zone';
}

//console.log(Util.inspect(exports.tenancy));

/******************************************************************************/

exports.mapping = {
  ghost: {
    alias: 'lobe',   classe: 'Lobe',   path: 'lobes/',
    pitch: [
      "Ensuring the ghost-line",
      "The lobe is ready to boot.",
      "The lobe has formed within the cyber-brain !-D"
    ],
  },
  brain: {
    alias: 'cortex', classe: 'Cortex', path: 'cortex/',
    pitch: [
      "Extending the brain",
      "The cortex is ready to boot.",
      "The cortex is hooked to the cyber-brain !-D"
    ],
    enrich: function (context) {
      /*
      exports.tenancy.topology = exports.tenancy.params['ann'];

      delete exports.tenancy.params['ann'];

      context['topology'] = exports.tenancy.topology;

      console.log("\t-> Topology   : "+exports.tenancy.topology);
      //*/

      return context;
    },
  },
  body: {
    alias: 'organ',  classe: 'Organ',  path: 'organs/',
    pitch: [
      "Preparing a new organ",
      "The organ is ready to boot.",
      "The engine is hooked to the Hub and serving well !-D"
    ],
  },
};

exports.meta = exports.mapping[exports.tenancy.protocol];

//console.log(Util.inspect(exports.meta));

/******************************************************************************/

var NeuroChip = require(__dirname+'/chip/__index.js');

NeuroChip.core.junction.connect({
  url:   'ws://127.0.0.1:7000/endpoint',
  realm: 'hub.'+exports.tenancy.domain,
  debug: true,
}, function (conn) {
  NeuroChip.core.junction.conn = conn;

  var context = {
    unit: exports.tenancy.identity,
    name: exports.tenancy.module,
  };

  //****************************************************************************

  console.log("*) " + exports.meta.pitch[0] + " : '"+exports.tenancy.module+"' :")

  console.log("\t-> Domain     : "+exports.tenancy.domain);
  console.log("\t-> Identity   : "+exports.tenancy.identity);
  console.log("\t-> Module     : "+exports.tenancy.module);

  if (exports.meta.enrich) {
    context = exports.meta.enrich(context);
  }

  console.log("\t-> Parameters : "+Util.inspect(exports.tenancy.params));

  //****************************************************************************

  var cls = NeuroChip.core[exports.meta.classe];

  exports[exports.meta.alias] = new cls(context, __dirname+'/chip/contrib/'+exports.meta.path, this.tenancy);

  /*
  switch (exports.tenancy.protocol) {
    case 'ghost':
      console.log("*) Ensuring the ghost-line : '"+exports.tenancy.module+"' :")

      console.log("\t-> Domain     : "+exports.tenancy.domain);
      console.log("\t-> Identity   : "+exports.tenancy.identity);
      console.log("\t-> Module     : "+exports.tenancy.module);
      console.log("\t-> Parameters : "+Util.inspect(exports.tenancy.params));

      exports.lobe = new NeuroChip.core.Lobe({
        unit:     exports.tenancy.identity,
        name:     exports.tenancy.module,
      }, __dirname+'/chip/contrib/'+);
      break;
    //**************************************************************************
    case 'brain':
      console.log("*) Extending the brain : '"+exports.tenancy.module+"' :")

      console.log("\t-> Domain     : "+exports.tenancy.domain);
      console.log("\t-> Identity   : "+exports.tenancy.identity);
      console.log("\t-> Alias      : "+exports.tenancy.module);
      console.log("\t-> Topology   : "+exports.tenancy.topology);
      console.log("\t-> Parameters : "+Util.inspect(exports.tenancy.params));

      exports.cortex = new NeuroChip.core.Cortex({
        unit:     exports.tenancy.identity,
        name:     exports.tenancy.module,
        topology: exports.tenancy.topology,
      }, __dirname+'/chip/contrib/cortex/');
      break;

      //************************************************************************

      console.log("#) The organ is ready to boot.")
      break;
    //**************************************************************************
    case 'body':
      console.log("*) Preparing a new organ : '"+exports.tenancy.module+"' :")

      console.log("\t-> Domain     : "+exports.tenancy.domain);
      console.log("\t-> Identity   : "+exports.tenancy.identity);
      console.log("\t-> Parameters : "+Util.inspect(exports.tenancy.params));

      exports.organ = new NeuroChip.core.Organ({
        unit: exports.tenancy.identity,
        name: exports.tenancy.module,
      }, __dirname+'/chip/contrib/organs/');
      break;

      //************************************************************************

      console.log("#) The organ is ready to boot.")
    //**************************************************************************
    default:
      console.log("Protocol '"+exports.tenancy.protocol+"' not supported !");
      break;
  }
  //*/

  console.log("#) "+exports.meta.pitch[1]);
}, function (session) {
  exports[exports.meta.alias].setup(session);

  //****************************************************************************

  exports[exports.meta.alias].publish('online', [exports.tenancy]);

  console.log("   ... connected to the Hub (y)")

  console.log(exports.meta.pitch[2])

  /*
  switch (exports.tenancy.protocol) {
    case 'ghost':
      exports.lobe.setup(session);

      //****************************************************************************

      console.log("The cortex is hooked to the cyber-brain !-D")
      break;
    case 'brain':
      exports.cortex.setup(session);

      //****************************************************************************

      console.log("The cortex is hooked to the cyber-brain !-D")
      break;
    case 'body':
      exports.organ.setup(session);

      //****************************************************************************

      console.log("The engine is hooked to the Hub and serving well !-D")
      break;
  }
  //*/

  //****************************************************************************

  exports[exports.meta.alias].forever();

  /*
  switch (exports.tenancy.protocol) {
    case 'ghost':
      exports.lobe.forever();
      break;
    case 'brain':
      exports.cortex.forever();
    case 'body':
      exports.organ.forever();
      break;
  }
  //*/
}, function () {
  console.log("#) We've been disconnected from the Hub (y)")

  exports[exports.meta.alias].publish('offline', [exports.tenancy]);

  exports[exports.meta.alias] = null;
});
