What is this ?
==============

Neurochip is, both, a node.js package & a Docker container. It provides a production-ready environment for custom machine-learning setups.

How to use it ?
===============

You can either, download the package and use it via Node.js using :

```bash
sudo npm install -g git+https://bitbucket.org/neurotics/neurchip.git
```

Or, run it through a container in Docker, by running :

```bash
docker pull neurotics/neurochip

docker run neurotics/neurochip --name sample-brain -it bash
```

Ghost :
-------
* Realm : nodejs main.js ghost://self/memory/facts
* Reason: nodejs main.js ghost://self/reasonner
* Concur: nodejs main.js ghost://self/concurrent
* Deduct: nodejs main.js ghost://self/deduce

Brain :
-------
* Lobes: nodejs main.js brain://self/ann/synaptic/perceptron?id=cortex
* Lymbe: nodejs main.js brain://self/assess/markov?id=lymbic
* Dorse: nodejs main.js brain://self/mapping/self-organised?id=dorsal

Body :
------
* Eye: nodejs main.js body://self/perceive/eye?id=1
* Ear: nodejs main.js "body://self/perceive/ear?id=1&google-api="

Online :
--------
* Hipchat: nodejs main.js "body://self/online/hippy?api_key=&api_secret="
* Slack:   nodejs main.js "body://self/online/slacking?api_key=&api_secret="
