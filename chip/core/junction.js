var autobahn = require('autobahn');

exports.connect = function (config, constructor, callback) {
  var conn = new autobahn.Connection(config);

  constructor(conn);

  conn.onopen = function (session) {
    callback(session);
  };

  return conn;
};
