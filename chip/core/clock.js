// create a hypertimer running ten times faster than real-time
var timer1 = hypertimer({rate: 10});

// retrieve the current configuration
console.log(timer1.config());  // returns an Object {rate: 10}

// create a hypertimer with the default rate (1 by default, same speed as real-time)
var timer2 = hypertimer();

// adjust the rate later on
timer2.config({rate: 1/2});

// create a hypertimer running discrete events as fast as possible (unpaced)
// (time will jump from scheduled event to scheduled event)
var timer3 = hypertimer({paced: false});

// create a hypertimer running discrete events with non-deterministic behavior
var timer4 = hypertimer({paced: false, deterministic: false});

/******************************************************************************/

// create a hypertimer with the initial time at 14st of February 2015
var timer = hypertimer({time: '2015-01-14T12:00:00.000Z'});

// change the time to the 1st of January 2050
timer.config({time: '2050-01-01T12:00:00.000Z'});

// get the time as Date
console.log(timer.getTime());  // Returns a date, Sat Jan 01 2050 12:00:00 GMT+0100 (CET)

// get the time as timestamp
console.log(timer.now());      // Returns a number, 2524647600000

/******************************************************************************/

var timer = hypertimer();

// pause the timer
timer.pause();

// time stands still here...
console.log(timer.running);  // false

// continue again
timer.continue();

// time is running again
console.log(timer.running);  // true

/******************************************************************************/

// create a hypertimer running ten times faster than real-time,
// start the timer at 1st of January 2050
var timer = hypertimer({
  rate: 10,
  time: new Date(2050, 0, 1, 12, 0, 0)
});

console.log('start', timer.getTime());      // start Sat Jan 01 2050 12:00:00 GMT+0100 (CET)

// set a timeout after a delay
var delay = 10000; // milliseconds (hyper-time)
var id1 = timer.setTimeout(function () {
  console.log('timeout', timer.getTime());  // timeout Sat Jan 01 2050 12:00:10 GMT+0100 (CET)
}, delay);

// set a timeout at a specific time
var time = new Date(2050, 0, 1, 12, 0, 20); // time (hyper-time)
var id2 = timer.setTrigger(function () {
  console.log('trigger', timer.getTime());  // trigger Sat Jan 01 2050 12:00:20 GMT+0100 (CET)
}, time);

// set an interval
var interval = 5000; // milliseconds (hyper-time)
var firstTime = new Date(2050, 0, 1, 12, 0, 30); // optional first time (hyper-time)
var counter = 0;
var id3 = timer.setInterval(function () {
  console.log('interval', timer.getTime()); // interval, 12:00:30, 12:00:35, 12:00:40, etc ...

  // cancel the interval after 10 times
  counter++;
  if (counter > 10) {
    timer.clearInterval(id3);
  }
}, interval, firstTime);

/******************************************************************************/

// create a hypertimer running discrete events
var timer = hypertimer({paced: false});

var delay = 10000;
timer.setTimeout(function () {
  console.log('Timeout A');

  timer.setTimeout(function () {
    console.log('Timeout B');
  }, delay);
}, delay);

// Will immediately output:
//   Timeout A
//   Timeout B

/******************************************************************************/

// asynchronous timeout
timer.setTimeout(function (done) {
  // ... do something
  done(); // call done when done
}, delay);

/******************************************************************************/

// create a hypertimer running discrete events,
// jumping from event to the next event.
var timer = hypertimer({paced: false});

// create an asynchronous timeout, having a callback parameter done
timer.setTimeout(function (done) {
  console.log('Timeout A');

  // perform an asynchronous action inside the timeout
  someAsyncAction(param, function (err, result) {
    timer.setTimeout(function () {
      console.log('Timeout B');
    }, 10000);

    // once we are done with our asynchronous event, call done()
    // so the hypertimer knows it can continue with the next event.
    done();
  });
}, 10000);

// Output:
//   Timeout A
//   (async action is executed here)
//   Timeout B

/******************************************************************************/

// create a master hypertimer listening on port 8081
var masterTimer = hypertimer({port: 8081});
// create a slave hypertimer connected to the master on port 8081
var slaveTimer = hypertimer({master: 'ws://localhost:8081'});

// close all connections, clear all timeouts
masterTimer.destroy();
slaveTimer.destroy();
