var celery = require('node-celery'),
    client = celery.createClient({
        CELERY_BROKER_URL: 'amqp://guest:guest@localhost:5672//',
        CELERY_RESULT_BACKEND: 'amqp'
    });

client.on('error', function(err) {
    console.log(err);
});

client.on('connect', function() {
    client.call('tasks.echo', ['Hello World!'], function(result) {
        console.log(result);
        client.end();
    });
});

var celery = require('node-celery'),
    client = celery.createClient({
        CELERY_BROKER_OPTIONS: {
            host: 'localhost',
            port: '5672',
            login: 'guest',
            password: 'guest',
            authMechanism: 'AMQPLAIN',
            vhost: '/',
            ssl: {
                enabled: true,
                keyFile: '/path/to/keyFile.pem',
                certFile: '/path/to/certFile.pem',
                caFile: '/path/to/caFile.pem'
            }
        },
        CELERY_RESULT_BACKEND: 'amqp'
    });


var celery = require('node-celery'),
client = celery.createClient({
    CELERY_BROKER_URL: 'amqp://guest:guest@localhost:5672//',
});

client.on('connect', function() {
    client.call('send-email', {
        to: 'to@example.com',
        title: 'sample email'
    }, {
        eta: new Date(Date.now() + 60 * 60 * 1000) // an hour later
    });
});

var celery = require('node-celery'),
    client = celery.createClient({
        CELERY_BROKER_URL: 'amqp://guest:guest@localhost:5672//',
    });

client.on('connect', function() {
    client.call('tasks.sleep', [2 * 60 * 60], null, {
        expires: new Date(Date.now() + 60 * 60 * 1000) // expires in an hour
    });
});

var celery = require('node-celery'),
    client = celery.createClient({
        CELERY_BROKER_URL: 'amqp://guest:guest@localhost:5672//',
        CELERY_RESULT_BACKEND: 'redis://localhost/0'
    });

client.on('connect', function() {
    var result = client.call('tasks.add', [1, 2]);
    setTimout(function() {
        result.get(function(data) {
            console.log(data); // data will be null if the task is not finished
        });
    }, 2000);
});

var celery = require('node-celery'),
    client = celery.createClient({
        CELERY_BROKER_URL: 'amqp://guest:guest@localhost:5672//',
        CELERY_ROUTES: {
            'tasks.send_mail': {
                queue: 'mail'
            }
        }
    }),
    send_mail = client.createTask('tasks.send_mail'),
    calculate_rating = client.createTask('tasks.calculate_rating');

client.on('error', function(err) {
    console.log(err);
});

client.on('connect', function() {
    send_mail.call([], {
        to: 'to@example.com',
        title: 'hi'
    }); // sends a task to the mail queue
    calculate_rating.call([], {
        item: 1345
    }); // sends a task to the default queue
});
