// exports.clock    = require('./clock.js');
exports.junction = require('./junction.js');
// exports.workers  = require('./workers.js');
// exports.crawler  = require('./crawler.js');

/******************************************************************************/

exports.Organ = function (nrw, pth) {
  this.narrow   = nrw;

  this.spine    = null;
  this.assembly = require(pth + this.narrow.name + '.js');

  this.nerves   = [];

  exports.junction.conn.open();
}

exports.Organ.prototype.namespace = function (value) { return 'agent.' + this.narrow.unit + (value==null ? '' : '.' + value); };
exports.Organ.prototype.prefix    = function (value) { return this.namespace('organ.' + this.narrow.name + (value==null ? '' : '.' + value)); };

exports.Organ.prototype.setup     = function (session) {
  this.spine = session;
  this.assembly.session = session;
  this.assembly.organ = this;

  //this.spine.publish(this.namespace('events.organs'), ['Hello, world!']);

  if (this.assembly.setup) {
    this.assembly.setup();
  }

  //this.spine.publish(this.namespace('events.organs'), ['Hello, world!']);
};

exports.Organ.prototype.forever   = function () {
  if (this.assembly.forever) {
    this.assembly.forever();
  } else if (this.assembly.loop) {
    setInterval(this.assembly.loop, this.assembly.cadence || 700);
  }
};

exports.Organ.prototype.connect   = function (nrw) {
  var nrv = new Nerve(this, nrw);

  //this.spine.publish(this.namespace('spine.nerves'), ['Hello, world!']);

  this.nerves.push(nrv);

  return nrv;
};

exports.Organ.prototype.notify    = function (args) {
  return this.spine.publish(this.namespace('events'), [this.prefix(), args]);
};

exports.Organ.prototype.subscribe = function (channel, callback) {
  this.notify(['subscribe', channel]);

  return this.spine.subscribe(this.prefix(channel), callback);
};

exports.Organ.prototype.publish = function (channel, args) {
  this.notify(['publish', channel, args]);

  return this.spine.publish(this.prefix(channel), args);
};

exports.Organ.prototype.register = function (method, callback) {
  this.notify(['register', method]);

  return this.spine.register(this.prefix(method), callback);
};

exports.Organ.prototype.call = function (method, args) {
  this.notify(['call', method, args]);

  return this.spine.call(this.prefix(method), args);
};

/******************************************************************************/

exports.Nerve = function (organ, narrow) {
  this.organ  = organ;
  this.narrow = narrow;
}

exports.Nerve.prototype.namespace = function (value) { return this.exports.Organ.namespace('spine.' + this.exports.Organ.narrow + '.' + this.narrow + (value==null ? '' : '.' + value)); };
exports.Nerve.prototype.prefix    = function (value) { return this.namespace(value); };

exports.Nerve.prototype.notify    = function (args) {
  return this.exports.Organ.spine.publish(this.namespace('events'), [this.prefix(), args]);
};

exports.Nerve.prototype.subscribe = function (channel, callback) {
  this.notify(['subscribe', channel]);

  return this.exports.Organ.spine.subscribe(this.prefix(channel), callback);
};

exports.Nerve.prototype.publish = function (channel, args) {
  this.notify(['publish', channel, args]);

  return this.exports.Organ.spine.publish(this.prefix(channel), args);
};

exports.Nerve.prototype.register = function (method, callback) {
  this.notify(['register', method]);

  return this.exports.Organ.spine.register(this.prefix(method), callback);
};

exports.Nerve.prototype.call = function (method, args) {
  this.notify(['call', method, args]);

  return this.exports.Organ.spine.call(this.prefix(method), args);
};

/******************************************************************************/

exports.Cortex = function (nrw, pth, tenant) {
  this.narrow   = nrw;

  this.spine    = null;
  this.assembly = require(pth + this.narrow.name + '.js');

  this.assembly.tenancy = tenant;

  this.nerves   = [];
  this.lobes    = [];

  exports.junction.conn.open();
}

exports.Cortex.prototype.namespace = function (value) { return 'agent.' + this.narrow.unit + (value==null ? '' : '.' + value); };
exports.Cortex.prototype.prefix    = function (value) { return this.namespace('cortex.' + this.narrow.name + (value==null ? '' : '.' + value)); };

/******************************************************************************/

exports.Cortex.prototype.setup     = function (session) {
  this.spine = session;
  this.assembly.session = session;
  this.assembly.cortex = this;

  //this.spine.publish(this.namespace('events.organs'), ['Hello, world!']);

  if (this.assembly.setup) {
    this.assembly.setup();
  }

  //this.spine.publish(this.namespace('events.organs'), ['Hello, world!']);

  var vectors = [
    'listing', 'get', 'exists', 'create',
    'train', 'stimulate',
  ];

  for (var i=0 ; i<vectors.length ; i++) {
    this.spine.subscribe(this.prefix(vectors[i]), this[vectors[i]]);
  }
};

exports.Cortex.prototype.forever   = function () {
  if (this.assembly.forever) {
    this.assembly.forever();
  } else if (this.assembly.loop) {
    setInterval(this.assembly.loop, this.assembly.cadence || 700);
  }
};

/******************************************************************************/

exports.Cortex.prototype.notify    = function (args) {
  return this.spine.publish(this.namespace('events'), [this.prefix(), args]);
};

/******************************************************************************/

exports.Cortex.prototype.subscribe = function (channel, callback) {
  this.notify(['subscribe', channel]);

  return this.spine.subscribe(this.prefix(channel), callback);
};

exports.Cortex.prototype.publish = function (channel, args) {
  this.notify(['publish', channel, args]);

  return this.spine.publish(this.prefix(channel), args);
};

exports.Cortex.prototype.register = function (method, callback) {
  this.notify(['register', method]);

  return this.spine.register(this.prefix(method), callback);
};

exports.Cortex.prototype.call = function (method, args) {
  this.notify(['call', method, args]);

  return this.spine.call(this.prefix(method), args);
};

/******************************************************************************/

exports.Cortex.prototype.ann = function (narrow) {
  for (var i=0 ; i<this.lobes.length ; i++) {
    if (exports.networks[i].narrow==narrow) {
      return exports.networks[i].instance;
    }
  }

  return null;
};

/******************************************************************************/

exports.Cortex.prototype.get = function (narrow) {
  for (var i=0 ; i<this.lobes.length ; i++) {
    if (exports.networks[i].narrow==narrow) {
      return exports.networks[i].serialize();
    }
  }

  return null;
};

/******************************************************************************/

exports.Cortex.prototype.exists = function (narrow) {
  var resp = this.get(narrow);

  if (resp!=null) {
    return true;
  } else {
    return false;
  }
};

/******************************************************************************/

exports.Cortex.prototype.create = function (narrow, config) {
  var resp = this.get(narrow);

  if (resp==null) {
    resp = new this.assembly.Network(narrow, config);

    this.lobes.push(resp);
  }

  return resp;
};

/******************************************************************************/

exports.Cortex.prototype.listing = function () {
  var resp = [];

  for (var i=0 ; i<this.lobes.length ; i++) {
    resp.push(exports.networks[i].serialize());
  }

  return resp;
};

/******************************************************************************/
/******************************************************************************/

exports.Cortex.prototype.train = function (narrow, payload, config) {
  var ann = this.ann_get(narrow);

  if (ann!=null) {
    ann.train(payload, config);
  }
};

/******************************************************************************/

exports.Cortex.prototype.stimulate = function (narrow, payload) {
  var ann = exports.get(narrow);

  if (ann!=null) {
    return ann.run(payload);
  } else {
    return null;
  }
};

/******************************************************************************/

exports.Lobe = function (nrw, pth) {
  this.narrow   = nrw;

  this.spine    = null;
  this.assembly = require(pth + this.narrow.name + '.js');

  this.nerves   = [];
  this.lobes    = [];

  exports.junction.conn.open();
}

exports.Lobe.prototype.namespace = function (value) { return 'agent.' + this.narrow.unit + (value==null ? '' : '.' + value); };
exports.Lobe.prototype.prefix    = function (value) { return this.namespace('lobe.' + this.narrow.name + (value==null ? '' : '.' + value)); };

/******************************************************************************/

exports.Lobe.prototype.setup     = function (session) {
  this.spine = session;
  this.assembly.session = session;
  this.assembly.cortex = this;

  //this.spine.publish(this.namespace('events.organs'), ['Hello, world!']);

  if (this.assembly.setup) {
    this.assembly.setup();
  }

  //this.spine.publish(this.namespace('events.organs'), ['Hello, world!']);

  var vectors = [
    'listing', 'get', 'exists', 'create',
    'train', 'stimulate',
  ];

  for (var i=0 ; i<vectors.length ; i++) {
    this.spine.subscribe(this.prefix(vectors[i]), this[vectors[i]]);
  }
};

exports.Lobe.prototype.forever   = function () {
  if (this.assembly.forever) {
    this.assembly.forever();
  } else if (this.assembly.loop) {
    setInterval(this.assembly.loop, this.assembly.cadence || 700);
  }
};

/******************************************************************************/

exports.Lobe.prototype.notify    = function (args) {
  return this.spine.publish(this.namespace('events'), [this.prefix(), args]);
};

/******************************************************************************/

exports.Lobe.prototype.subscribe = function (channel, callback) {
  this.notify(['subscribe', channel]);

  return this.spine.subscribe(this.prefix(channel), callback);
};

exports.Lobe.prototype.publish = function (channel, args) {
  this.notify(['publish', channel, args]);

  return this.spine.publish(this.prefix(channel), args);
};

exports.Lobe.prototype.register = function (method, callback) {
  this.notify(['register', method]);

  return this.spine.register(this.prefix(method), callback);
};

exports.Lobe.prototype.call = function (method, args) {
  this.notify(['call', method, args]);

  return this.spine.call(this.prefix(method), args);
};
