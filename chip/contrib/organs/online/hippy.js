var hipchat = require('node-hipchat');

var HC = new hipchat('YOUR_API_KEY');

HC.listRooms(function(data) {
  console.log(data); // These are all the rooms
});

var params = {
  room: 123456, // Found in the JSON response from the call above
  from: 'FunkyMonkey',
  message: 'Some HTML <strong>formatted</strong> string',
  color: 'yellow'
};

HC.postMessage(params, function(data) {
  // Message has been sent!
});
