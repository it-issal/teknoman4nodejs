var Speakable = require('speakable');

var speakeasy = require("speakeasy-nlp");

exports.setup = function (session) {
  exports.nerve = exports.organ.connect(session, 'acoustic');

  exports.nerve.register('listen', function (args) {
    exports.handler.recordVoice();
  });

  exports.nerve.subscribe('hear', function (args) {
    var sentence = args[0];

    console.log("Self-Talk:", sentence);

    exports.nerve.publish('feel', [
      sentence,
      speakeasy.sentiment.analyze(sentence),
      speakeasy.classify(sentence),
    ]);
  });

  exports.handler = new Speakable({key: 'your-google-API-key'}, {lang: 'en-EN'});

  exports.handler.on('speechStart', function() {
    exports.nerve.publish(exports.prefix+'.events', ['Speech Start']);
  });

  exports.handler.on('speechStop', function() {
    exports.nerve.publish(exports.prefix+'.events', ['Speech Stop']);
  });

  exports.handler.on('speechReady', function() {
    exports.nerve.publish(exports.prefix+'.events', ['Speech Ready']);
  });

  exports.handler.on('error', function(err) {
    exports.nerve.publish(exports.prefix+'.errors', [err]);
  });

  exports.handler.on('speechResult', function (recognizedWords) {
    exports.nerve.publish('hear', [recognizedWords]);

    exports.nerve.call('listen', [2, 3]).then(function (res) {
      console.log("Result:", res);
    });
  });
};

exports.loop = function () {
  /*
  exports.nerve.call('listen', [2, 3]).then(function (res) {
    console.log("Result:", res);
  });
  //*/
};
