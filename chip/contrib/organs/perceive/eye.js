exports.cadence = 250;

var cv = require('opencv');
//var Faced = require('faced');

//var faced = new Faced();

var callbacks = {
  counter: {
    face: 0,
    haar: 0,
  },
  show: function (im)  {
    console.log(im.size());

    if (im.size()[0] > 0 && im.size()[1] > 0){
      exports.window.show(im);
    }
    exports.window.blockingWaitKey(0, 50);
  },
  face: function (im)  {
    im.detectObject(cv.FACE_CASCADE, {}, function(err, faces){
      if (err) {
        console.log(err);
      } else {
        for (var i=0;i<faces.length; i++){
          var x = faces[i];

          im.ellipse(x.x + x.width/2, x.y + x.height/2, x.width/2, x.height/2);
        }

        var pth = 'media/eye/face-'+callbacks.counter.face+'.png'; callbacks.counter.face += 1; im.save(pth); console.log('Image saved to '+pth);
      }
    });
  },
  haar: function (im)  {
    if (im.width() < 1 || im.height() < 1) throw new Error('Image has no size');

    im.detectObject("/usr/share/opencv/haarcascades/haarcascade_frontalface_alt2.xml", {}, function(err, faces) {
      if (err) {
        console.log(err);
      } else {
        var resp = [];

        for (var i = 0; i < faces.length; i++){
            var face = faces[i];

            im.ellipse(face.x + face.width / 2, face.y + face.height / 2, face.width / 2, face.height / 2);

            resp.push({
              left:   face.x,
              top:    face.y,
              width:  face.width,
              height: face.height,
            });
        }

        //exports.nerve.publish('seen', ['faces', faces]);

        //callbacks.show(im);

        if (0 < faces.length) {
          exports.organ.publish('seen', ['faces', resp]);
        }

        //var pth = 'media/eye/haar-'+callbacks.counter.haar+'.png'; callbacks.counter.haar += 1; im.save(pth); console.log('Image saved to '+pth);
      }
    });
  },
  faced: function () {
    faced.detect('image.jpg', function (faces, image, file) {
      if (!faces) {
        return console.log("No faces found!");
      }

      var face = sface[0];

      console.log(
        "Found a face at %d,%d with dimensions %dx%d",
        face.getX(),
        face.getY(),
        face.getWidth(),
        face.getHeight()
      );

      console.log(
        "What a pretty face, it %s a mouth, it %s a nose, it % a left eye and it %s a right eye!",
        face.getMouth() ? "has" : "does not have",
        face.getNose() ? "has" : "does not have",
        face.getEyeLeft() ? "has" : "does not have",
        face.getEyeRight() ? "has" : "does not have",
      );
    });
  },
};

exports.setup = function (session) {
  /*
  exports.nerve = exports.organ.connect(session, 'optic');

  exports.nerve.subscribe('seen', function (args) {
    console.log("\t-> Seen '", args[0], "' : ", args[1]);
  });

  exports.nerve.register('track', function (args) {
    exports.handler.recordVoice();
  });
  //*/

  try {
    exports.camera = new cv.VideoCapture(0);
    exports.window = new cv.NamedWindow('Eye Preview', 0);
  } catch (e){
    console.log("#) Couldn't start camera:", e)
  }
};

exports.loop = function () {
  exports.camera.read(function(err, im) {
    if (err) {
      console.log(err);
    } else {
      callbacks.haar(im);
    }
  });
};
