var rdfstore = require('rdfstore');

/******************************************************************************/

exports.setup = function (session) {
  rdfstore.create(function(err, store) {
      exports.store = store;
  });

  /*
  exports.nerve = exports.organ.connect(session, 'acoustic');

  exports.nerve.register('listen', function (args) {
    exports.handler.recordVoice();
  });

  exports.nerve.subscribe('hear', function (args) {
    var sentence = args[0];

    console.log("Self-Talk:", sentence);

    exports.nerve.publish('feel', [
      sentence,
      speakeasy.sentiment.analyze(sentence),
      speakeasy.classify(sentence),
    ]);
  });
  //*/
};

exports.loop = function () {
  //exports.organ.publish('seen', ['hallucination']);
};

exports.load = {
  // jsonld @:( alias="ex", ns="http://example.org/people/", uid="ex:test", payload=null )
  jsonld: function (alias, ns, uid, payload) {
      payload = {
          "@context": {
              "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
              "xsd": "http://www.w3.org/2001/XMLSchema#",
              "name": "http://xmlns.com/foaf/0.1/name",
              "age": {
                  "@id": "http://xmlns.com/foaf/0.1/age",
                  "@type": "xsd:integer",
              },
              "homepage": {
                  "@id": "http://xmlns.com/foaf/0.1/homepage",
                  "@type": "xsd:anyURI",
              },
              "ex": "http://example.org/people/"
          },
          "@id": "ex:john_smith",
          "name": "John Smith",
          "age": "41",
          "homepage": "http://example.org/home/",
      };

      exports.store.setPrefix(alias, ns);

      exports.store.load("application/ld+json", payload, uid, function(err,results) {
          store.node("ex:john_smith", "ex:test", function(err, graph) {
              // process graph here
          });
      });
  },
  // named @:( alias='lisp', link='http://dbpedialite.org/titles/Lisp_%28programming_language%29' )
  named: function (alias, link) {
     exports.store.execute('LOAD <'+link+'> INTO GRAPH <'+alias+'>', function(err){
          if(err) {
          }
      });
  },
};

exports.query = {
  // named @:( alias='lisp', fields='?o', expr='{ ?s foaf:page ?o} }', prefix='foaf:<http://xmlns.com/foaf/0.1/>' )
  named: function (alias, fields, expr, prefix) {
      exports.store.execute('PREFIX '+prefix+' SELECT '+fields+' FROM NAMED <'+alias+'> { GRAPH <'+alias+'> }'+stmt, function(err, results) {
          // process results
      });
  },
  // rdf @:( stmt="SELECT * { ?s ?p ?o }" )
  rdf: function (stmt) {
      exports.store.execute(stmt, function(err, results){
          if(!err) {
              // process results
              if(results[0].s.token === 'uri') {
                  console.log(results[0].s.value);
              }
          }
      });
  },
  // sparql @:( stmt="SELECT * { ?s ?p ?o }" )
  sparql: function (stmt) {
      exports.store.execute(stmt, function(err, results){
          if(!err) {
              // process results
              if(results[0].s.token === 'uri') {
                  console.log(results[0].s.value);
              }
          }
      });
  },
};

/******************************************************************************/

if (false) {
  var defaultGraph = [{'token':'uri', 'value': graph1}, {'token':'uri', 'value': graph2}];
  var namedGraphs  = [{'token':'uri', 'value': graph3}, {'token':'uri', 'value': graph4}];

  store.executeWithEnvironment("SELECT * { ?s ?p ?o }",defaultGraph,
    namedGraphs, function(err, results) {
    if(err) {
      // process results
    }
  });

  /******************************************************************************/

  var query = "CONSTRUCT { <http://example.org/people/Alice> ?p ?o } \
               WHERE { <http://example.org/people/Alice> ?p ?o  }";

  store.execute(query, function(err, graph){
    if(graph.some(store.rdf.filters.p(store.rdf.resolve('foaf:name')))) {
      nameTriples = graph.match(null,
                                store.rdf.createNamedNode(rdf.resolve('foaf:name')),
                                null);

      nameTriples.forEach(function(triple) {
        console.log(triple.object.valueOf());
      });
    }
  });

  /******************************************************************************/

  /* retrieving a whole graph as JS Interafce API graph object */

  store.graph(graphUri, function(graph){
    // process graph
  });


  /* Exporting a graph to N3 (this function is not part of W3C's API)*/
  store.graph(graphUri, function(graph){
    var serialized = graph.toNT();
  });


  /* retrieving a single node in the graph as a JS Interface API graph object */

  store.node(subjectUri, function(graph) {
    //process node
  });

  store.node(subjectUri, graphUri, function(graph) {
    //process node
  });



  /* inserting a JS Interface API graph object into the store */

  // inserted in the default graph
  store.insert(graph, function(err) {}) ;

  // inserted in graphUri
  store.insert(graph, graphUri, function(err) {}) ;



  /* deleting a JS Interface API graph object into the store */

  // deleted from the default graph
  store.delete(graph, function(err){});

  // deleted from graphUri
  store.delete(graph, graphUri, function(err){});



  /* clearing a graph */

  // clears the default graph
  store.clear(function(err){});

  // clears a named graph
  store.clear(graphUri, function(err){});



  /* Parsing and loading a graph */

  // loading local data
  store.load("text/turtle", turtleString, function(err, results) {});

  // loading remote data
  store.load('remote', remoteGraphUri, function(err, results) {});



  /* Registering a parser for a new media type */

  // The parser object must implement a 'parse' function
  // accepting the data to parse and a callback function.

  store.registerParser("application/rdf+xml", rdXmlParser);

  /******************************************************************************/

  var graph = store.rdf.createGraph();
  graph.addAction(rdf.createAction(store.rdf.filters.p(store.rdf.resolve("foaf:name")),
                                   function(triple){ var name = triple.object.valueOf();
                                                     var name = name.slice(0,1).toUpperCase()
                                                     + name.slice(1, name.length);
                                                     triple.object = store.rdf.createNamedNode(name);
                                                     return triple;}));

  store.rdf.setPrefix("ex", "http://example.org/people/");
  graph.add(store.rdf.createTriple( store.rdf.createNamedNode(store.rdf.resolve("ex:Alice")),
                                    store.rdf.createNamedNode(store.rdf.resolve("foaf:name")),
                                    store.rdf.createLiteral("alice") ));

  var triples = graph.match(null, store.rdf.createNamedNode(store.rdf.resolve("foaf:name")), null).toArray();

  console.log("worked? "+(triples[0].object.valueOf() === 'Alice'));

  /******************************************************************************/

  new Store({name:'test', overwrite:true}, function(err,store){
      store.execute('INSERT DATA {  <http://example/person1> <http://xmlns.com/foaf/0.1/name> "Celia" }', function(err){

         store.registerDefaultProfileNamespaces();

         store.execute('SELECT * { ?s foaf:name ?name }', function(err,results) {
             test.ok(results.length === 1);
             test.ok(results[0].name.value === "Celia");
         });
      });
  });

  /******************************************************************************/

  var cb = function(event, triples){
    // it will receive a notifications where a triple matching
    // the pattern s:http://example/boogk, p:*, o:*, g:*
    // is inserted or removed.
    if(event === 'added') {
      console.log(triples.length+" triples have been added");
    } else if(event === 'deleted') {
      console.log(triples.length+" triples have been deleted");
    }
  }

  store.subscribe("http://example/book",null,null,null,cb);


  // .. do something;

  // stop receiving notifications
  store.unsubscribe(cb);

  /******************************************************************************/

  var cb = function(node){
    // it will receive the updated version of the node each
    // time it is modified.
    // If the node does not exist, the graph received will
    // not contain triples.
    console.log("The node has now "+node.toArray().length+" nodes");
  }

  // if only tow arguments are passed, the default graph will be used.
  // A graph uri can be passed as an optional second argument.
  store.startObservingNode("http://example/book",cb);


  // .. do something;

  // stop receiving notifications
  store.stopObservingNode(cb);

  /******************************************************************************/

  new Store({name:'test', overwrite:true}, function(err,store) {
      store.load(
          'text/n3',
          '@prefix test: <http://test.com/> .\
           test:A test:prop 5.\
           test:B test:prop 4.\
           test:C test:prop 1.\
           test:D test:prop 3.',
          function(err) {

              var invoked = false;
              store.registerCustomFunction('my_addition_check', function(engine,args) {
          // equivalent to var v1 = parseInt(args[0].value), v2 = parseInt(args[1]);

          var v1 = engine.effectiveTypeValue(args[0]);
          var v2 = engine.effectiveTypeValue(args[1]);

          // equivalent to return {token: 'literal', type:"http://www.w3.org/2001/XMLSchema#boolean", value:(v1+v2<5)};

          return engine.ebvBoolean(v1+v2<5);
      });

         store.execute(
                  'PREFIX test: <http://test.com/> \
                   SELECT * { ?x test:prop ?v1 .\
                              ?y test:prop ?v2 .\
                              filter(custom:my_addition_check(?v1,?v2)) }',
                  function(err) {
                     test.ok(results.length === 3);
             for(var i=0; i<results.length; i++) {
              test.ok(parseInt(results[i].v1.value) + parseInt(results[i].v2.value) < 5 );
          }
          test.done()
          }
      );
    });
  });
}
