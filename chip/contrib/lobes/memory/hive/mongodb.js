var MongoClient = require('mongodb').MongoClient, test = require('assert');
var url = 'mongodb://localhost:27017/test';

MongoClient.connect(url, function(err, db) {
  // Use the admin database for the operation
  var adminDb = db.admin();

  // List all the available databases
  adminDb.listDatabases(function(err, dbs) {
    test.equal(null, err);
    test.ok(dbs.databases.length > 0);
    db.close();
  });
});

MongoClient.connect(url, function(err, db) {
  // Create a collection we want to drop later
  var col = db.collection('createIndexExample1');
  // Insert a bunch of documents
  col.insert([{a:1, b:1}
    , {a:2, b:2}, {a:3, b:3}
    , {a:4, b:4}], {w:1}, function(err, result) {
    test.equal(null, err);

// Show that duplicate records got dropped
col.aggregation({}, {cursor: {}}).toArray(function(err, items) {
  test.equal(null, err);
  test.equal(4, items.length);
  db.close();
});

  });
});

MongoClient.connect(url, function(err, db) {
  // Create a collection we want to drop later
  var col = db.collection('createIndexExample1');
  // Show that duplicate records got dropped
  col.find({}).toArray(function(err, items) {
    test.equal(null, err);
    test.equal(4, items.length);
    db.close();
  });
});

MongoClient.connect(url, function(err, db) {
  // Create a collection we want to drop later
  var col = db.collection('listCollectionsExample1');
  // Insert a bunch of documents
  col.insert([{a:1, b:1}
    , {a:2, b:2}, {a:3, b:3}
    , {a:4, b:4}], {w:1}, function(err, result) {
      test.equal(null, err);

      // List the database collections available
      db.listCollections().toArray(function(err, items) {
        test.equal(null, err);
        db.close();
      });
  });
});

MongoClient.connect(url, function(err, db) {
  // Create a collection we want to drop later
  var col = db.collection('createIndexExample1');
  // Insert a bunch of documents
  col.insert([{a:1, b:1}
    , {a:2, b:2}, {a:3, b:3}
    , {a:4, b:4}], {w:1}, function(err, result) {
    test.equal(null, err);

    // Show that duplicate records got dropped
    col.find({}).toArray(function(err, items) {
      test.equal(null, err);
      test.equal(4, items.length);
      db.close();
    });
  });
});

MongoClient.connect(url, function(err, db) {
  var testDb = db.db('test');
  db.close();
});

MongoClient.connect(url, function(err, db) {
  var gridStore = new GridStore(db, null, "w");
  gridStore.open(function(err, gridStore) {
    gridStore.write("hello world!", function(err, gridStore) {
      gridStore.close(function(err, result) {
        GridStore.read(db, result._id, function(err, data) {
          test.equal('hello world!', data);
          db.close();
          test.done();
        });
      });
    });
  });
});

MongoClient.connect(url, function(err, db) {
  var gridStore = new GridStore(db, null, "w");
  gridStore.open(function(err, gridStore) {
    gridStore.write("hello world!", function(err, gridStore) {
      gridStore.close(function(err, result) {
        // Let's read the file using object Id
        GridStore.read(db, result._id, function(err, data) {
          test.equal('hello world!', data);
          db.close();
          test.done();
        });
      });
    });
  });
});
