var DecisionTree = require('decision-tree');

exports.data = {
  training: [
      { "color":"blue",   "shape":"square",  "liked":false},
      { "color":"red",    "shape":"square",  "liked":false},
      { "color":"blue",   "shape":"circle",  "liked":true},
      { "color":"red",    "shape":"circle",  "liked":true},
      { "color":"blue",   "shape":"hexagon", "liked":false},
      { "color":"red",    "shape":"hexagon", "liked":false},
      { "color":"yellow", "shape":"hexagon", "liked":true},
      { "color":"yellow", "shape":"circle",  "liked":true}
  ],
  testing: [
      { "color":"blue",   "shape":"hexagon", "liked":false},
      { "color":"red",    "shape":"hexagon", "liked":false},
      { "color":"yellow", "shape":"hexagon", "liked":true},
      { "color":"yellow", "shape":"circle",  "liked":true}
  ],
};

exports.setup = function (session) {
  exports.instance = new DecisionTree(exports.data.training, 'liked', ["color", "shape"]);

  exports.organ.register('concure', function (args) {
    var predicted_class = exports.instance.predict(args[0]);

    exports.organ.publish('tournament', [exports.instance.results()]);

    return predicted_class;
  });
};

exports.loop = function () {
  console.log('Decision making accuracy :=> ', exports.instance.evaluate(exports.data.testing));

  console.log('Decision tree for blue hexagon : ', exports.organ.call('concure', [{
      color: "blue",
      shape: "hexagon"
  }]));

  console.log('Decision making accuracy :=> ', exports.instance.evaluate(exports.data.testing));
};
