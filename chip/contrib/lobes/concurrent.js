var Duel = require('duel');

exports.setup = function (session) {
  exports.instance = new Duel(4); // 4 players - single elimination

  exports.organ.register('concure', function (args) {
    exports.instance.matches.forEach(function (m) {
      exports.instance.score(m.id, args[0][m.id] || args[1]);
    });

    exports.organ.publish('tournament', [exports.instance.results()]);
  });
};

exports.loop = function () {
  exports.organ.call('concure', [{
    1: [0, 1],
  }, [1, 0]]);
};
