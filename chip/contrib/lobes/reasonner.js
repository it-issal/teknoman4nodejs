var SparqlClient = require('sparql-client');

exports.setup = function (session) {
  /*
  exports.nerve = exports.organ.connect(session, 'acoustic');

  exports.nerve.register('listen', function (args) {
    exports.handler.recordVoice();
  });

  exports.nerve.subscribe('hear', function (args) {
    var sentence = args[0];

    console.log("Self-Talk:", sentence);

    exports.nerve.publish('feel', [
      sentence,
      speakeasy.sentiment.analyze(sentence),
      speakeasy.classify(sentence),
    ]);
  });
  //*/
};

exports.loop = function () {
  //exports.organ.publish('seen', ['hallucination']);
};

exports.query = function (endpoint, query, binding, callback) {
  var client = new SparqlClient(endpoint);

  console.log("Query to " + endpoint);
  console.log("Query: " + query);

  var qs = client.query(query);

  if (binding) {
    qs = binding(qs);
  }

  qs.execute(callback);

  return qs;
};

exports.test = function () {
  var util = require('util');

  exports.query('http://dbpedia.org/sparql',
    "SELECT * FROM <http://dbpedia.org> WHERE { ?city <http://dbpedia.org/property/leaderName> ?leaderName } LIMIT 10",
    function (query) {
      query = query.bind('city', '<http://dbpedia.org/resource/Vienna>');

      //query = query.bind('city', 'db:Chicago');
      //query = query.bind('city', 'db:Tokyo');
      //query = query.bind('city', 'db:Casablanca');

      return query;
    },
    function(error, results) {
      process.stdout.write(util.inspect(arguments, null, 20, true)+"\n");1
    }
  );
};
