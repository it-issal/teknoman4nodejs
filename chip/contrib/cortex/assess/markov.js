var util = require('util');
var fs = require('fs');

var markov = require('markov');

exports.setup = function (session) {
  exports.instance = markov(1);

  exports.organ.subscribe('hear', function (args) {
    var sentence = args[0];

    exports.lobe.publish('guts', exports.instance.respond(sentence));

    exports.instance.seed(sentence, function () {
      // console.log("\t-> <feel-ya> { ", sentence, " }");
    });

    exports.lobe.publish('hench', exports.instance.respond(sentence));
  });
};

exports.loop = function () {
  exports.lobe.publish('voices', exports.instance.pick());
};
