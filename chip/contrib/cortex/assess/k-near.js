var generator = require('knear');

var k = 3; //k can be any integer
var machine = new generator.kNear(k);

machine.learn([-1,2,3],'good');
machine.learn([0,0,0],'good');
machine.learn([10,10,10],'bad');
machine.learn([9,12,9],'bad');

machine.classify([1,0,1]);
//returns 'good'

machine.classify([11,11,9]);
//returns 'bad'var generator = require('knear');

var k = 3; //k can be any integer
var machine = new generator.kNear(k);

machine.learn([-1,2,3],'good');
machine.learn([0,0,0],'good');
machine.learn([10,10,10],'bad');
machine.learn([9,12,9],'bad');

machine.classify([1,0,1]);
//returns 'good'

machine.classify([11,11,9]);
//returns 'bad'
