var classifier = require('classifier');

exports.setup = function (session) {
  exports.instance = new classifier.Bayesian({
    backend: {
      type: 'Redis',
      options: { hostname: 'localhost', port: 6379, name: 'emailspam' }
    }
  });

  /*
  exports.organ.register('concure', function (args) {
    exports.instance.matches.forEach(function (m) {
      exports.instance.score(m.id, args[0][m.id] || args[1]);
    });

    exports.organ.publish('tournament', [exports.instance.results()]);
  });
  //*/

  exports.instance.train("cheap replica watches", 'spam', function() {
    console.log("trained");
  });

};

exports.loop = function () {
  exports.instance.classify("free watches", function(category) {
    console.log("classified in: " + category);

    //exports.organ.publish('tournament', [exports.instance.results()]);
  });
};
