var FANN = require('fann');

/******************************************************************************/

exports.Network = function (narrow, config) {
  this.narrow = narrow;
  this.config = config;

  this.instance = new FANN.standard(2,3,1);
};

/******************************************************************************/

exports.Network.prototype.train = function (payload, config) {
  return this.instance.train(payload, config);
};

/******************************************************************************/

exports.Network.prototype.stimulate = function (payload, callback) {
  var thrill = this.instance.run(payload);

  if (callback) {
    callback(payload, thrill);
  } else {
    this.diffuse(callback, payload, thrill);
  }
};

/******************************************************************************/

exports.Network.prototype.diffuse = function (payload, thrill) {

};

/******************************************************************************/

exports.Network.prototype.serialize = function () {
  var resp = {
    narrow: this.narrow,
    config: this.config,
  };

  return resp;
};

/******************************************************************************/
/******************************************************************************/

exports.setup = function (session) {
  exports.create('test');

  exports.train('test', [
    [[0, 0], [0]],
    [[0, 1], [1]],
    [[1, 0], [1]],
    [[1, 1], [0]],
  ], {
    error: 0.00001,
  });
};

exports.loop = function () {
  console.log("xor test (0,0) -> ", exports.simulate('test', [0, 0]));
  console.log("xor test (1,0) -> ", exports.simulate('test', [1, 0]));
  console.log("xor test (0,1) -> ", exports.simulate('test', [0, 1]));
  console.log("xor test (1,1) -> ", exports.simulate('test', [1, 1]));
};
