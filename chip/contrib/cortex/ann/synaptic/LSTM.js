var synaptic = require('synaptic'); // this line is not needed in the browser
var Neuron = synaptic.Neuron,
    Layer = synaptic.Layer,
    Network = synaptic.Network,
    Trainer = synaptic.Trainer,
    Architect = synaptic.Architect;

/******************************************************************************/

exports.setup = function () {
  exports.network = new Architect.LSTM(2,4,4,4,1);; // create a network for 10-bit patterns

  /*
  // teach the network two different patterns
  exports.network.learn([
      [0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
      [1, 1, 1, 1, 1, 0, 0, 0, 0, 0]
  ])

  console.log(exports.network.feed([0,1,0,1,0,1,0,1,1,1])); // [0, 1, 0, 1, 0, 1, 0, 1, 0, 1]
  console.log(exports.network.feed([1,1,1,1,1,0,0,1,0,0])); // [1, 1, 1, 1, 1, 0, 0, 0, 0, 0]
  //*/
};

exports.loop = function () {

};
