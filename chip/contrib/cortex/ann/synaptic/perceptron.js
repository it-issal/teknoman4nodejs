var synaptic = require('synaptic'); // this line is not needed in the browser
var Neuron = synaptic.Neuron,
    Layer = synaptic.Layer,
    Network = synaptic.Network,
    Trainer = synaptic.Trainer,
    Architect = synaptic.Architect;

/******************************************************************************/

exports.Network = function (narrow, config) {
  this.narrow = narrow;
  this.config = config;

  this.instance = new Architect.Perceptron(2,3,1);
  this.trainer = new Trainer(this.instance);
};

/******************************************************************************/

exports.Network.prototype.train = function (payload, config) {
  var hnd = this.trainer[payload];

  return hnd(config);
};

/******************************************************************************/

exports.Network.prototype.stimulate = function (payload, callback) {
  var thrill = this.instance.activate(payload);

  if (callback) {
    callback(payload, thrill);
  } else {
    this.diffuse(callback, payload, thrill);
  }
};

/******************************************************************************/

exports.Network.prototype.diffuse = function (payload, thrill) {

};

/******************************************************************************/

exports.Network.prototype.serialize = function () {
  var resp = {
    narrow: this.narrow,
    config: this.config,
  };

  return resp;
};

/******************************************************************************/
/******************************************************************************/

exports.setup = function (session) {
  exports.create('test');
};

exports.loop = function () {
  exports.train('test', 'XOR');

  console.log("xor test (0,0) -> ", exports.simulate('test', [0, 0]));
  console.log("xor test (0,1) -> ", exports.simulate('test', [0, 1]));
  console.log("xor test (1,0) -> ", exports.simulate('test', [1, 0]));
  console.log("xor test (1,1) -> ", exports.simulate('test', [1, 1]));
};
